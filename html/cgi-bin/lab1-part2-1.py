#!/usr/bin/python

# Import modules for CGI handling
import cgi, cgitb
import os

print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "<title>Hello - headers</title>"
print "</head>"
print "<body>"
print "<h3>HTTP_CONNECTION: %s </h3>" % (os.environ['HTTP_CONNECTION'])
print "<h3>REQUEST_METHOD: %s %s </h3>" % (os.environ['REQUEST_METHOD'],os.environ['REQUEST_URI'])
print "<h3>HTTP_ACCEPT: %s </h3>" % (os.environ['HTTP_ACCEPT'])
print "<h3>HTTP_USER_AGENT: %s </h3>" % (os.environ['HTTP_USER_AGENT'])
print "<h3>HTTP_ACCEPT_ENCODING: %s </h3>" % (os.environ['HTTP_ACCEPT_ENCODING'])
print "<h3>HTTP_CACHE_CONTROL: %s </h3>" % (os.environ['HTTP_CACHE_CONTROL'])
print "<h3>HTTP_ACCEPT_LANGUAGE: %s </h3>" % (os.environ['HTTP_ACCEPT_LANGUAGE'])
print "<h3>HTTP_HOST: %s </h3>" % (os.environ['HTTP_HOST'])
print "<h3>QUERY_STRING: %s </h3>" % (os.environ['QUERY_STRING'])
print "</body>"
print "</html>"