#!/usr/bin/python

# Import modules for CGI handling
import cgi, cgitb
import os

query = os.environ['QUERY_STRING']

location = '/index.html'
if query == 'google':
        location = 'http://www.google.com'
elif query == 'bing':
        location = 'http://www.bing.com'

print "HTTP/1.1 302 Found"
print "Location: ",location,"\r\n"
print "Connection: close \r\n"
print ""

print # to end the CGI response headers.