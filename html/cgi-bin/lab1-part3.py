#!/usr/bin/python

# Import modules for CGI handling
import cgi, cgitb
import os

content_type = os.environ['CONTENT_TYPE']
accept_header = os.environ['HTTP_ACCEPT']

if content_type == 'application/json' :
	if accept_header == 'application/vnd.byu.cs462.v1+json':
		print "HTTP/1.1 200 OK"
		print
		print '{"version": "v1" }'

	elif accept_header == 'application/vnd.byu.cs462.v2+json':
			print "HTTP/1.1 200 OK"
			print
			print '{"version": "v2" }'
else:
	print "HTTP/1.1 404 Not Found"
	print "Connection: close \r\n"
	print ""	

	print # to end the CGI response headers.

